import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner le = new Scanner(System.in);

        System.out.print("Digite o valor do lado A: ");
        double a = le.nextDouble();
        System.out.print("Digite o valor do lado B: ");
        double b = le.nextDouble();
        System.out.print("Digite o valor do lado C: ");
        double c = le.nextDouble();

        double perimetro = a + b + c;
        double metade_perimetro = perimetro / 2;
        double area = Math.sqrt(metade_perimetro * (metade_perimetro - a) * (metade_perimetro - b) * (metade_perimetro - c));
        if (a == b && b == c) {
            System.out.println("Triângulo Equilátero");
        } else if (a != b && b != c && a != c) {
            System.out.println("Triângulo Escaleno");
        } else {
            System.out.println("Triângulo Isósceles");
        }
        System.out.printf("Área: %.2f\n", area);
        System.out.printf("Perímetro: %.2f\n", perimetro);
    }
}
