import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner le = new Scanner(System.in);
        System.out.print("Digite o valor de A: ");
        int a = le.nextInt();
        System.out.print("Digite o valor de B: ");
        int b = le.nextInt();
        for (int i = a; i <= b; i++) {
            if (i % 2 != 0) {
                System.out.println(i);
            }
        }
    }
}